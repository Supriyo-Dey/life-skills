# Listening and assertive communication

## 1. Steps/Strategies for Active Listening

- Give the speaker your full attention
- Avoid interrupting
- Show empathy and understanding by nodding or using verbal cues like "I see" or "I understand."
- Paraphrase and summarize the speaker's message.

## 2. Key Points of Reflective Listening (Fisher's Model)

Reflective listening, according to Fisher's model, involves mirroring the speaker's body language, emotions, and verifying the message. It is commonly used in critical situations like in military or airports.

## 3. Obstacles in Listening Process

Obstacles in the listening process can include:
- Distractions (external or internal).
- Preconceived notions or biases.
- Lack of focus and attention.
- Ineffective communication by the speaker

## 4. Improving Listening Skills

To improve listening skills, we can do:
- Minimize distractions.
- Practice active listening techniques
- Be aware of personal biases and judgments that can hinder understanding.
- Focus on the speaker's message without interrupting or formulating responses prematurely.
- Seek clarification and ask questions to ensure comprehension.

## 5. Switching to Passive Communication Style

Passive communication style may be adopted when individuals avoid conflict, suppress their needs, or lack assertiveness to express themselves. Examples can be situations where someone fails to express their true feelings due to fear of rejection.

## 6. Switching to Aggressive Communication Style

Aggressive communication style may occur when individuals use forceful and dominating approaches, such as confrontations, verbal attacks, or disregarding others' rights. Examples can be situations where someone becomes confrontational or uses intimidation to assert their position.

## 7. Passive Aggressive Communication Styles

Passive-aggressive communication involves indirect expression of negative emotions, such as sarcasm, gossip, taunts, or the silent treatment. Examples include making snide remarks or spreading rumors to convey displeasure.

## 8. Making Communication Assertive

To make communication assertive, consider the following steps:
- Clearly express thoughts, needs, and feelings using "I" statements.
- Maintain a calm and confident demeanor while being respectful.
- Listen actively and seek to understand others' perspectives.
- Set boundaries and assert rights without infringing on others' rights.
- Practice effective problem-solving and compromise in conflicts.
- Use assertive body language, such as maintaining eye contact and confident posture.