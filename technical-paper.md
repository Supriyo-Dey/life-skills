# Apache Kafka

![Apache Kafka Logo](images/ApacheKafka.png)


## What is Kafka?
Apache Kafka is an Open source, distributed, streaming system for stream processing, real time data pipelines and data integration at scale. It is based on principle of distributed computing, enabling high throughput, low latency and being highly scalable. It uses topics, which are immutable logs that are distributed and replicated in clusters, which contain multiple servers called brokers. Consumers can either access a particular log, or a whole topic as per their needs, consumers can also subscribe to this data.

If you aim to develop a scalable, real time data streaming application, the tool you are looking for is going to be Apache Kafka.

![Apache Kafka Diagram](images/Kafka-Working.png)


## Origins of Kafka
Apache Kafka was named after a very enigmatic and influential figures in literature Franz Kafka. Apache Kafka was originally created by LinkedIn in 2011. It was written using Java and Scala and was later open soured by Apache software foundation. It was originally built to be a higly scalable and fault tolerant messaging system to handle large amounts of data.




## How Kafka Works

Various things happen in our surroundings are called events, these events are measured and calculated and data is stored in logs, which are basically an ordered sequence of events. Apache Kafka provides this data to end tools with addition of various features such as high availability and high durability at no cost of latency to the tool, thus, it can be said that Apache Kakfa is a tool to manage these logs at scale.

A collection of logs is called a topic, which is an ordered collection of events stored in a durable way into clusters with multiple brokers, who act as point of contact for those who want to accessthe data from the storage and it provides them with the topics and data required to them .

These logs are stored and streamed by Apache Kafka to various tools and applications that have subscribed to this data.

Apache Kafka encourages us to think events first.

Apache Kafka can:
- Perform Real time processing at scale
- provide Durable and persistent storage
- provide publish and subscribe functionalities to the user.

Apache Kafka is used for:
- Data Pipelines
- Stream Processing
- Streaming analytics
- Streaming ETL

## Advantages
There are various advantages of using Apache Kafka such as :
- High throughput
- Highly scalable
- Low Latency
- Durable and Permanent storage
- High Availability

## Streams API
Apache Kafka provides Streams API as a functionality to its users which can be used to transform and aggregate these topics before they even reach the end user or the consumer. It can also be used to filter and map topic data and can be used to perform mapping on it.


## Usage in Industry
Apache Kafka is used by a large amount of industry leading companies to manage their high streams of data, here are a few:

- Netflix
- LinkedIn
- Microsoft
- AirBnB
- Target
- Lyft


## References

1. [YouTube - Introduction to Apache Kafka](https://www.youtube.com/watch?v=06iRM1Ghr1k)
2. [Confluent - What is Apache Kafka?](https://www.confluent.io/what-is-apache-kafka/)
3. [Red Hat - What is Apache Kafka?](https://www.redhat.com/en/topics/integration/what-is-apache-kafka)
4. [Apache Kafka - Medium Article](https://levelup.gitconnected.com/apache-kafka-c78b13ffdc39)
