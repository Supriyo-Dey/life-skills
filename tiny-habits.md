# Tiny Habits

1. **In this video, what was the most interesting story or idea for you?**  
In this video BJ Fogg explains how small changes clumped together over a period of time can make impactful and meaningful changes to ones life trajectory.
An interesting story that speaker told in this video was about how he lost weight over a period of time by just starting small by weighing himself each day.

2. **How can you use B = MAP to make making new habits easier? What are M, A and P.**  
The formula B=MAP is proposed by Fogg, and is thus called BJ-Fogg behaviour model, here,
* B - Behaviour
* M - Motivation
* A - Ability
* P - Prompt
We can use B-MAP formula to improve our chances to stick to new behaviour by starting small and making it easier to get into and slowly building habit from there.


3. **Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)**  
It is important to celebrate after each success because :
* Provides positive reinforcement.
* Provides an emotional connection to the habit
* Builds self confidence.

4. **In this video, what was the most interesting story or idea for you?**
An interesting story from the the video is about a friend of the speaker and how she optimized for starting line by making a habit to wake up, freshen up and get a cab, that was her morning routine, but this caused her to go to the gym each day.

5. **What is the book's perspective about Identity?**  
The book "Atomic Habits" by James Clear emphasizes that identity is the foundation of lasting habit change. It also conveys that Small Wins Shape Identity of an individual.


6. **Write about the book's perspective on how to make a habit easier to do?**  
There are four laws from the book's perspective on how to make a habit easier to do:
* Make it Obvious
* Make it Attractive
* Make it Easy
* Make it Satisfying

7. **Write about the book's perspective on how to make a habit harder to do?**  
According to the book, to make a habit harder to do:
* Make it Invisible
* Make it Unattractive
* Make it Difficult
* Make it Unsatisfying

8. **Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**  
A tiny habit I would like to build is of starting to work out each day. To make the habit more appealing and attractive to me, I would like eto start small by working out each day for 10 minutes each and slowly build from there by providing positive reinforcements after each session.

9. **Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?**  
One habit I would like to eliminate is the habit of procastination, there are several steps that can be taken to make the process unattractive such as adding a 5 minute run each time I procastinate a task.

