# Sexual Harassment: Behavior

Sexual harassment can occur in many different ways and is a severe problem. It is imperative to acknowledge that any conduct that is unwanted, unsuitable, or creates a hostile atmosphere on the basis of gender may qualify as sexual harassment. Typical instances include the following:

1. **Unwanted sexual comments or advances:** This could include explicit remarks, jokes, or gestures.
2. **Sexual coercion:** Pressuring someone into sexual activities against their will.
3. **Inappropriate touching or groping:** Any unwanted physical contact of a sexual nature.


# Responding to Sexual Harassment

If you witness or experience any incidents of sexual harassment, here are some steps you can consider taking:

1. **Stay Safe:** Ensure your immediate safety and remove yourself from the situation if necessary.

2. **Document the Incident:** Keep a record of the incidents, including dates, times, locations, and descriptions of what occurred. If possible, gather any supporting evidence, such as text messages or emails.

3. **Speak Up:** If you feel uncomfortable, clearly and assertively communicate to the harasser that their behavior is unwelcome and needs to stop. Sometimes, people may not be aware that their actions are causing discomfort.

4. **Report to Authorities or Human Resources:** If the harassment persists, report the incidents to your supervisor, human resources department, or another appropriate authority within your organization. Follow any established reporting procedures.

5. **Seek Support:** Reach out to friends, family, or colleagues for emotional support. It's important to share your experience with someone you trust.

6. **Legal Action:** If the harassment continues and is not addressed appropriately, you may consider seeking legal advice or filing a complaint with relevant authorities against the harasser.