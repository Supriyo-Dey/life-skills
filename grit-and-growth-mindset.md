# Grit and Growth Mindset

1. **Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.**
Grit is the ability of a person to have the courage or resolve to face challenges and get back up when they fail in the said challenges and work harder than previous time to face the challenge head on.

2. **Paraphrase (summarize) the video in a few (1 or 2) lines in your own words**
This video explains how people with growth mindset, who believe that skills and abilities are built by hardwork of an individual far outperform those with a fixed mindset in the long run.

3. **What is the Internal Locus of Control? What is the key point in the video?**
Locus of control of an individual is the degree to which he/she believes they have control over their lives. Individuals with an internal locus of control perform better than those with external locus of control.

4. **What are the key points mentioned by speaker to build growth mindset (explanation not needed).**
Some of the points mentioned by speaker are:
    * Believe in your ability to figure things out.
    * Question your assumptions.
    * Create your own curriculum for growth.
    * Honor the struggle.

5. **What are your ideas to take action and build Growth Mindset?**
My idea to build growth mindset aree:
    * Keep an open mind to oncoming challenges and face them with courage.
    * Be open to being confronted to my mistakes and areas for improvement.
    * Always look for an oppurtunity to learn from each challenge.